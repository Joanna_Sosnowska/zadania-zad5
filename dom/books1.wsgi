#!/usr/bin/python
# -*- coding=utf-8 -*-
__author__ = 'Joanna667'

import urlparse
import sys
#sys.path.insert(0, "/home/p14/www/wsgi")
from bookdb import BookDB
from wsgiref.simple_server import make_server
from cgi import escape

home = "194.29.175.240/~p14/www/wsgi/books.wsgi"
body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Baza książek w WSGI</title>
        <style>
        a{
        color: #89a820;
        }
        a:hover{
        color:#1e1e1e;
        }
        </style>
    </head>
    <body>
"""
def ksiazka(environ, start_response, numer):
    ksiazka = BookDB.title_info(numer)
    odp = body + ("<h1>Książka numer: {0} </h1> <body>").format(numer)

    odp += "Tytuł : " + ksiazka['title'] + "</br>" \
                     "ISBN : " + ksiazka['isbn'] + "</br>" \
                     "Wydawca : " + ksiazka['publisher'] + "</br>" \
                     "Autor : " + ksiazka['author'] + "</br></br>" \
                     "<a href='http://"+home+"'>Powrót</a></br></br>" \

    odp += """</body></html>"""
    status = '200 OK'
    odp_header = [('Content-Type', 'text/html'), ('Content-Length', str(len(odp)))]
    start_response(status, odp_header)
    return [odp]


def error(environ, start_response):
    odp = body + "<h1>404 - pozycja nie istnieje</h1>"
    odp += "</body></html>"
    status = '404 Not Found'
    odp_header = [('Content-Type', 'text/html'), ('Content-Length', str(len(odp)))]
    start_response(status, odp_header)
    return [odp]


def application(environ, start_response):
    path = environ['PATH_INFO']
    if "id" in path:
        try:
            return ksiazka(environ, start_response, int(path[-1]))
        except:
            return error(environ, start_response)
    else:
        d = urlparse.parse_qs(environ.get('QUERY_STRING', 'Unset'))
        idd = d.get('id', [''])
        #strona z listą książek

        resp_body = body
        try:
            ctn = False
            for i in BookDB().titles():
                if i['id'][2:] == idd[0]:
                    resp_body += "<h2><a href='http://"+home+"'>Powrót</a></h2>"
                    resp_body += "<h4>Tytuł: </h4><h3>"+BookDB().title_info('id'+idd[0])['title']+"</h3>"
                    resp_body += "<h4>ISBN: </h4><h3>"+BookDB().title_info('id'+idd[0])['isbn']+"</h3>"
                    resp_body += "<h4>Publisher: </h4><h3>"+BookDB().title_info('id'+idd[0])['publisher']+"</h3>"
                    resp_body += "<h4>Author: </h4><h3>"+BookDB().title_info('id'+idd[0])['author']+"</h3>"
                    ctn = True
     
            if not ctn:
                for i in BookDB().titles():
                    resp_body += "<h3>"+i['id']+": <a href='http://"+home+"/?id="+i['id'][2:]+"'>"+i['title']+"</a></h3>"
     
            resp_body += "</body></html>"
    
            resp_status = '200 OK'

            resp_header = [('Content-Type', 'text/html'), ('Content-Length', str(len(resp_body)))]
            start_response(resp_status, resp_header)
            return [resp_body]

        except:
            return error(environ, start_response)




if __name__ == '__main__':
    server = make_server('194.29.175.240', 5000, application)
    server.serve_forever()
